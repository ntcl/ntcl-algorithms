module repeated_matrix_multiplication_test_helper_module
    use, intrinsic :: iso_fortran_env, only : &
            real32, &
            real64

    use :: util_api, only : &
            assert, &
            string, &
            dictionary

    use :: data_api, only : &
            storage_helper, &
            stream, &
            dependency_manager

    use :: tensor_api, only : &
            scalar, &
            matrix, &
            copy_tensor

    use :: algorithms_api, only : &
            matrix_multiplication, &
            matrix_multiplication_factory

    implicit none
    private

    public :: repeated_matrix_multiplication_test_helper

    type :: repeated_matrix_multiplication_test_helper
    contains
        procedure :: run => run
        procedure, nopass :: run_real32 => run_real32
        procedure, nopass :: run_real64 => run_real64
        procedure, nopass :: run_complex64 => run_complex64
        procedure, nopass :: run_complex128 => run_complex128
    end type repeated_matrix_multiplication_test_helper

    real(real32), parameter :: single = 1.0e-2
    real(real64), parameter :: double = 1.0d-11
    integer, parameter :: number_of_repetitions = 10
contains
    subroutine run(this, assertion, prefix, driver, memory_type, options, priorities)
        class(repeated_matrix_multiplication_test_helper), intent(in) :: this
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        call this%run_real32(assertion, prefix//"real32:", driver, memory_type, options, priorities)
        call this%run_real64(assertion, prefix//"real64:", driver, memory_type, options, priorities)
        call this%run_complex64(assertion, prefix//"complex64:", driver, memory_type, options, priorities)
        call this%run_complex128(assertion, prefix//"complex128:", driver, memory_type, options, priorities)
    end subroutine run

    subroutine run_real32(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(matrix) :: a, b, c
        integer :: m, n, k, rep_idx
        real(real32), dimension(:,:), allocatable :: am, bm, cm
        type(storage_helper) :: helper
        class(matrix_multiplication), allocatable :: mm
        type(stream) :: astream

        a = matrix(); b=matrix(); c=matrix()
        call matrix_multiplication_factory%create(mm, driver, options, priorities)

        m = 20; n = 30; k = 50
        allocate(am(m, k), bm(k, n), cm(m,n))
        call random_number(am)
        call random_number(bm)

        call copy_tensor(a, am, memory_type, options, priorities)
        call copy_tensor(b, bm, memory_type, options, priorities)

        astream = dependency_manager%get_new_stream()
        do rep_idx = 1, number_of_repetitions
            cm=0.0
            call copy_tensor(c, cm, memory_type, options, priorities)
            cm = matmul(am, bm)

            call mm%mm(c, a, b, astream=astream)
            call mm%synchronize(astream)
        end do
        call dependency_manager%destroy_stream(astream)
        call assertion%equal(prefix//"::Product of random matrices repeated", &
                helper%equal(c%storage, cm, single))

        call a%cleanup(); call b%cleanup(); call c%cleanup()

        call mm%cleanup()
        deallocate(mm)
        deallocate(am, bm, cm)
    end subroutine run_real32

    subroutine run_real64(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(matrix) :: a, b, c
        integer :: m, n, k, rep_idx
        real(real64), dimension(:,:), allocatable :: am, bm, cm
        type(storage_helper) :: helper
        class(matrix_multiplication), allocatable :: mm
        type(stream) :: astream

        a = matrix(); b=matrix(); c=matrix()
        call matrix_multiplication_factory%create(mm, driver, options, priorities)

        m = 2000; n = 3000; k = 5000
        allocate(am(m, k), bm(k, n), cm(m,n))
        call random_number(am)
        call random_number(bm)
        cm=0.0

        call copy_tensor(a, am, memory_type, options, priorities)
        call copy_tensor(b, bm, memory_type, options, priorities)
        call copy_tensor(c, cm, memory_type, options, priorities)

        astream = dependency_manager%get_new_stream()
        do rep_idx = 1, number_of_repetitions
            cm = matmul(am, bm)

            call mm%mm(c, a, b, astream=astream)
            call mm%synchronize(astream)
        end do
        call dependency_manager%destroy_stream(astream)
        call assertion%equal(prefix//"::Product of random matrices repeated", &
            helper%equal(c%storage, cm, double))

        call a%cleanup(); call b%cleanup(); call c%cleanup()

        call mm%cleanup()
        deallocate(mm)
        deallocate(am, bm, cm)
    end subroutine run_real64

    subroutine run_complex64(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(matrix) :: a, b, c
        integer :: m, n, k, rep_idx
        complex(real32), dimension(:,:), allocatable :: am, bm, cm
        real(real32), dimension(:,:), allocatable :: are, aim, bre, bim 
        type(storage_helper) :: helper
        class(matrix_multiplication), allocatable :: mm
        complex(real32) :: tolerance
        type(stream) :: astream

        a = matrix(); b=matrix(); c=matrix()
        tolerance = (single, single)
        call matrix_multiplication_factory%create(mm, driver, options, priorities)

        m = 2000; n = 3000; k = 5000
        allocate(am(m, k), bm(k, n), cm(m,n))
        allocate(are(m, k), aim(m, k))
        allocate(bre(k, n), bim(k, n))
        call random_number(are)
        call random_number(aim)
        call random_number(bre)
        call random_number(bim)
        am = cmplx(are, aim)
        bm = cmplx(bre, bim)
        cm=0.0

        call copy_tensor(a, am, memory_type, options, priorities)
        call copy_tensor(b, bm, memory_type, options, priorities)
        call copy_tensor(c, cm, memory_type, options, priorities)

        astream = dependency_manager%get_new_stream()
        do rep_idx = 1, number_of_repetitions
            cm = matmul(am, bm)

            call mm%mm(c, a, b, astream=astream)
            call mm%synchronize(astream)
        end do
        call dependency_manager%destroy_stream(astream)
        call assertion%equal(prefix//"::Product of random matrices repeated", &
                helper%equal(c%storage, cm, tolerance))

        call a%cleanup(); call b%cleanup(); call c%cleanup()

        call mm%cleanup()
        deallocate(mm)
        deallocate(am, bm, cm)
    end subroutine run_complex64

    subroutine run_complex128(assertion, prefix, driver, memory_type, options, priorities)
        type(assert), intent(inout) :: assertion
        character(len=*), intent(in) :: prefix
        character(len=*), intent(in), optional :: driver, memory_type
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(matrix) :: a, b, c
        integer :: m, n, k, rep_idx
        complex(real64), dimension(:,:), allocatable :: am, bm, cm
        real(real64), dimension(:,:), allocatable :: are, aim, bre, bim 
        type(storage_helper) :: helper
        class(matrix_multiplication), allocatable :: mm
        complex(real64) :: tolerance
        type(stream) :: astream

        a = matrix(); b=matrix(); c=matrix()
        tolerance = (double, double)
        call matrix_multiplication_factory%create(mm, driver, options, priorities)

        m = 2000; n = 3000; k = 5000
        allocate(am(m, k), bm(k, n), cm(m,n))
        allocate(are(m, k), aim(m, k))
        allocate(bre(k, n), bim(k, n))
        call random_number(are)
        call random_number(aim)
        call random_number(bre)
        call random_number(bim)
        am = cmplx(are, aim)
        bm = cmplx(bre, bim)
        cm=0.0

        call copy_tensor(a, am, memory_type, options, priorities)
        call copy_tensor(b, bm, memory_type, options, priorities)
        call copy_tensor(c, cm, memory_type, options, priorities)

        astream = dependency_manager%get_new_stream()
        do rep_idx = 1, number_of_repetitions
            cm = matmul(am, bm)

            call mm%mm(c, a, b, astream=astream)
            call mm%synchronize(astream)
        end do
        call dependency_manager%destroy_stream(astream)
        call assertion%equal(prefix//"::Product of random matrices repeated", &
                helper%equal(c%storage, cm, tolerance))

        call a%cleanup(); call b%cleanup(); call c%cleanup()

        call mm%cleanup()
        deallocate(mm)
        deallocate(am, bm, cm)
    end subroutine run_complex128
end module repeated_matrix_multiplication_test_helper_module
