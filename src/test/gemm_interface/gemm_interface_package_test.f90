! Auto-generated -- DO NOT MODIFY
module gemm_interface_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: gemm_comparing_with_blas_test_module, only : gemm_comparing_with_blas_test
    use :: calling_gemm_with_first_element_test_module, only : calling_gemm_with_first_element_test
    use :: repeated_calling_of_gemm_test_module, only : repeated_calling_of_gemm_test
    use :: gemm_large_matrix_test_module, only : gemm_large_matrix_test

    implicit none
    private

    public :: gemm_interface_package_test

    type :: gemm_interface_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type gemm_interface_package_test

    interface gemm_interface_package_test
        module procedure constructor
    end interface gemm_interface_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(gemm_interface_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(gemm_interface_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(gemm_comparing_with_blas_test) :: agemm_comparing_with_blas_test
        type(calling_gemm_with_first_element_test) :: acalling_gemm_with_first_element_test
        type(repeated_calling_of_gemm_test) :: arepeated_calling_of_gemm_test
        type(gemm_large_matrix_test) :: agemm_large_matrix_test

        call assertion%equal("gemm_interface::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("gemm_comparing_with_blas") ) then
            agemm_comparing_with_blas_test = gemm_comparing_with_blas_test()
            call agemm_comparing_with_blas_test%run(assertion)
            call agemm_comparing_with_blas_test%cleanup()
        end if

        if ( &
                this%test_selector%is_enabled("calling_gemm_with_first_element") ) then
            acalling_gemm_with_first_element_test = calling_gemm_with_first_element_test()
            call acalling_gemm_with_first_element_test%run(assertion)
            call acalling_gemm_with_first_element_test%cleanup()
        end if

        ! The following tests will not be run unless long is specified.
        if ( &
                this%test_selector%is_enabled("repeated_calling_of_gemm") .and. &
                this%test_selector%is_enabled("long") ) then
            arepeated_calling_of_gemm_test = repeated_calling_of_gemm_test()
            call arepeated_calling_of_gemm_test%run(assertion)
            call arepeated_calling_of_gemm_test%cleanup()
        end if

        if ( &
                this%test_selector%is_enabled("gemm_large_matrix") .and. &
                this%test_selector%is_enabled("long") ) then
            agemm_large_matrix_test = gemm_large_matrix_test()
            call agemm_large_matrix_test%run(assertion)
            call agemm_large_matrix_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(gemm_interface_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(gemm_interface_package_test), intent(inout) :: this
    end subroutine clear
end module gemm_interface_package_test_module
