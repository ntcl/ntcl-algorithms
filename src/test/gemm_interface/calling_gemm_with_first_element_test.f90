module calling_gemm_with_first_element_test_module
    use, intrinsic :: iso_fortran_env, only : real64

    use :: util_api, only : assert

    use :: gemm_interface_module, only : ntcl_gemm, ntcl_gemm_real64

    implicit none
    private

    public :: calling_gemm_with_first_element_test

    type :: calling_gemm_with_first_element_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type calling_gemm_with_first_element_test

    interface calling_gemm_with_first_element_test
        module procedure constructor
    end interface calling_gemm_with_first_element_test
contains
    function constructor() result(this)
        type(calling_gemm_with_first_element_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(calling_gemm_with_first_element_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        integer, parameter :: side = 10
        real(real64), dimension(side, side) :: A, B, C, C_expected

        call assertion%equal("calling_gemm_with_first_element::Test complete", .true.)
        
        call random_number(A) 
        call random_number(B) 
        C(:, :) = 0.0D0

        call ntcl_gemm_real64('N', 'N', side, side, side, 1.0D0, A(1,1), side, B(1,1), side, 0.0D0, C(1,1), side) 

        C_expected = matmul(A, B)

        call assertion%is_equal("calling_gemm_with_first_element::Compare with matmul", &
                                C, C_expected, 1.0D-6)
    end subroutine run

    subroutine cleanup(this)
        class(calling_gemm_with_first_element_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(calling_gemm_with_first_element_test), intent(inout) :: this
    end subroutine clear
end module calling_gemm_with_first_element_test_module
