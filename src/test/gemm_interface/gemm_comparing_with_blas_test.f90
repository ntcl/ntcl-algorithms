module gemm_comparing_with_blas_test_module
    use, intrinsic :: iso_fortran_env, only : real32, real64

    use, intrinsic :: ieee_arithmetic, only : ieee_is_nan

    use :: util_api, only : assert

    use :: gemm_interface_module, only : ntcl_gemm

    implicit none
    private

    public :: gemm_comparing_with_blas_test

    type :: gemm_comparing_with_blas_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type gemm_comparing_with_blas_test

    interface gemm_comparing_with_blas_test
        module procedure constructor
    end interface gemm_comparing_with_blas_test

    interface get_matrix_block
        procedure get_matrix_block_real32
        procedure get_matrix_block_real64
        procedure get_matrix_block_complex64
        procedure get_matrix_block_complex128
    end interface get_matrix_block

contains

    function constructor() result(this)
        type(gemm_comparing_with_blas_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(gemm_comparing_with_blas_test), intent(in) :: this
        type(assert), intent(inout) :: assertion
        integer, parameter :: number_of_repetitions_per_type = 10
        integer :: repetition_index
        character(len=256) :: repetition_name

        call assertion%equal("gemm_comparing_blas::Test complete", .true.)

        do repetition_index = 1, number_of_repetitions_per_type
            write (repetition_name, '(A37, I3, A9)') &
                    'gemm_comparing_blas::repetition::', repetition_index, &
                    ' complete'

            call test_random_matrices_real32(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real32(assertion, 'N', 'N', 5, 10, 10, 5, 10, 5)
            call test_random_matrices_real32(assertion, 'N', 'N', 10, 20, 20, 10, 20, 10)
            call test_random_matrices_real32(assertion, 'N', 'N', 5, 11, 11, 5, 11, 5)
            call test_random_matrices_real32(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real32(assertion, 'N', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real32(assertion, 'T', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real32(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)


            call test_random_matrix_slices_real32(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_real32(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_real32(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_with_skips_real32(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_real32(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_real32(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrices_real64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real64(assertion, 'N', 'N', 5, 10, 10, 5, 10, 5)
            call test_random_matrices_real64(assertion, 'N', 'N', 10, 20, 20, 10, 20, 10)
            call test_random_matrices_real64(assertion, 'N', 'N', 5, 11, 11, 5, 11, 5)
            call test_random_matrices_real64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real64(assertion, 'N', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real64(assertion, 'T', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_real64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_real64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_real64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_real64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_with_skips_real64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_real64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_real64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrices_complex64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex64(assertion, 'N', 'N', 5, 10, 10, 5, 10, 5)
            call test_random_matrices_complex64(assertion, 'N', 'N', 10, 20, 20, 10, 20, 10)
            call test_random_matrices_complex64(assertion, 'N', 'N', 5, 11, 11, 5, 11, 5)
            call test_random_matrices_complex64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex64(assertion, 'N', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex64(assertion, 'T', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_complex64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_complex64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_complex64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_with_skips_complex64(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_complex64(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_complex64(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrices_complex128(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex128(assertion, 'N', 'N', 5, 10, 10, 5, 10, 5)
            call test_random_matrices_complex128(assertion, 'N', 'N', 10, 20, 20, 10, 20, 10)
            call test_random_matrices_complex128(assertion, 'N', 'N', 5, 11, 11, 5, 11, 5)
            call test_random_matrices_complex128(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex128(assertion, 'N', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex128(assertion, 'T', 'T', 10, 10, 10, 10, 10, 10)
            call test_random_matrices_complex128(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_complex128(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_complex128(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_complex128(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)

            call test_random_matrix_slices_with_skips_complex128(assertion, 'N', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_complex128(assertion, 'T', 'N', 10, 10, 10, 10, 10, 10)
            call test_random_matrix_slices_with_skips_complex128(assertion, 'N', 'N', 5, 10, 10, 10, 10, 10)


            call assertion%equal(repetition_name, .true.)
        end do

    end subroutine run

    subroutine test_random_matrices_real32(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real32), dimension(:, :), allocatable :: A, B, matmul_C, ntcl_C
        real(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        character(len=256) :: test_name

        write (test_name, "(A41, A, A3, A, A2, I3, A, I3, A, I3, A12, I3, A3, I3, A11, I3, A)") &
                "gemm_comparing_blas::dgemm_real32('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A, ", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real32(A, transa, lda, m, k)
        call initialize_random_matrix_real32(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A, lda, B, ldb, beta, ntcl_C, ldc)
        matmul_C(1:m, 1:n) = alpha*matmul(get_matrix_block(A, m, k, transa /= 'n' .and. transa /= 'N'), &
                                get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrices_real32

    subroutine test_random_matrix_slices_real32(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real32), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        real(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 2*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/2
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/2
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A41, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real32_slice('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real32(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_real32(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + lda-1), &
                          A_column_offset:(A_column_offset + A_column_slice_length-1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + lda-1), &
                     A_column_offset:(A_column_offset + A_column_slice_length-1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_real32

    subroutine test_random_matrix_slices_with_skips_real32(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real32), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        real(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name, contains_nan_test

        A_number_of_rows = 4*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/4
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/4
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A52, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real32_slice_with_skips('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        write (contains_nan_test, "(A65, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real32_slice_with_skips_contains_nan('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real32(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_real32(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + 2*lda-1):2, &
                          A_column_offset:(A_column_offset + A_column_slice_length-1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + 2*lda-1):2, &
                     A_column_offset:(A_column_offset + A_column_slice_length-1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%equal(contains_nan_test, all(.not. ieee_is_nan(ntcl_C)))
        call assertion%equal(contains_nan_test, all(.not. ieee_is_nan(matmul_C)))
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_with_skips_real32

    subroutine test_random_matrices_real64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real64), dimension(:, :), allocatable :: A, B, matmul_C, ntcl_C
        real(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-8
        character(len=256) :: test_name

        write (test_name, "(A41, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real64('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real64(A, transa, lda, m, k)
        call initialize_random_matrix_real64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A, lda, B, ldb, beta, ntcl_C, ldc)
        matmul_C(1:m, 1:n) = alpha*matmul(get_matrix_block(A, m, k, transa /= 'n' .and. transa /= 'N'), &
                                get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, ntcl_C, matmul_C, tollerance)
        deallocate(A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrices_real64

    subroutine test_random_matrix_slices_real64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real64), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        real(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 2*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/2
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/2
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A41, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real64_slice('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real64(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_real64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + lda-1), &
                          A_column_offset:(A_column_offset + A_column_slice_length-1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + lda-1), &
                     A_column_offset:(A_column_offset + A_column_slice_length-1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_real64

    subroutine test_random_matrix_slices_with_skips_real64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        real(real64), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        real(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name, contains_nan_test

        A_number_of_rows = 4*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/4
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/4
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A52, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real64_slice_with_skips('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        write (contains_nan_test, "(A65, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_real64_slice_with_skips_contains_nan('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_real64(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_real64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_number(matmul_C)
        ntcl_C = matmul_C
        call random_number(alpha)
        call random_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + 2*lda-1):2, &
                          A_column_offset:(A_column_offset + A_column_slice_length-1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + 2*lda-1):2, &
                     A_column_offset:(A_column_offset + A_column_slice_length-1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%equal(contains_nan_test, all(.not. ieee_is_nan(ntcl_C)))
        call assertion%equal(contains_nan_test, all(.not. ieee_is_nan(matmul_C)))
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_with_skips_real64

    subroutine test_random_matrices_complex64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real32), dimension(:, :), allocatable :: A, B, matmul_C, ntcl_C
        complex(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        character(len=256) :: test_name

        write (test_name, "(A44, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex64('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex64(A, transa, lda, m, k)
        call initialize_random_matrix_complex64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex64_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex64_number(alpha)
        call random_complex64_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A, lda, B, ldb, beta, ntcl_C, ldc)
        matmul_C(1:m, 1:n) = alpha*matmul(get_matrix_block(A, m, k, transa /= 'n' .and. transa /= 'N'), &
                                get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrices_complex64

    subroutine test_random_matrix_slices_complex64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real32), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        complex(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 2*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/2
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/2
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A44, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex64_slice('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex64(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_complex64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex64_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex64_number(alpha)
        call random_complex64_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + lda - 1), &
                          A_column_offset:(A_column_offset + A_column_slice_length - 1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + lda - 1), &
                     A_column_offset:(A_column_offset + A_column_slice_length - 1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_complex64

    subroutine test_random_matrix_slices_with_skips_complex64(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real32), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        complex(real32) :: alpha, beta
        real(real32), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 4*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/4
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/4
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A55, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex64_slice_with_skips('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex64(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_complex64(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex64_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex64_number(alpha)
        call random_complex64_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + 2*lda - 1):2, &
                          A_column_offset:(A_column_offset + A_column_slice_length - 1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + 2*lda - 1):2, &
                     A_column_offset:(A_column_offset + A_column_slice_length - 1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_with_skips_complex64

    subroutine test_random_matrices_complex128(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real64), dimension(:, :), allocatable :: A, B, matmul_C, ntcl_C
        complex(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-5
        character(len=256) :: test_name

        write (test_name, "(A44, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex128('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex128(A, transa, lda, m, k)
        call initialize_random_matrix_complex128(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex128_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex128_number(alpha)
        call random_complex128_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A, lda, B, ldb, beta, ntcl_C, ldc)
        matmul_C(1:m, 1:n) = alpha*matmul(get_matrix_block(A, m, k, transa /= 'n' .and. transa /= 'N'), &
                                get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrices_complex128

    subroutine test_random_matrix_slices_complex128(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real64), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        complex(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 2*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/2
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/2
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A45, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex128_slice('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex128(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_complex128(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex128_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex128_number(alpha)
        call random_complex128_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + lda - 1), &
                          A_column_offset:(A_column_offset + A_column_slice_length - 1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + lda - 1), &
                     A_column_offset:(A_column_offset + A_column_slice_length - 1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, matmul_A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_complex128

    subroutine test_random_matrix_slices_with_skips_complex128(assertion, transa, transb, m, n, k, lda, ldb, ldc)
        type(assert), intent(inout) :: assertion
        character, intent(in) :: transa, transb
        integer, intent(in) :: n, m, k, lda, ldb, ldc
        complex(real64), dimension(:, :), allocatable :: A, matmul_A, B, matmul_C, ntcl_C
        complex(real64) :: alpha, beta
        real(real64), parameter :: tollerance = 1e-5
        integer :: A_number_of_rows, A_number_of_columns, A_row_offset, &
                   A_column_offset, A_column_slice_length
        character(len=256) :: test_name

        A_number_of_rows = 4*lda
        if (transa == 'N' .or. transa == 'n') then
            A_number_of_columns = 2*k
            A_row_offset = m/4
            A_column_offset = k/2
            A_column_slice_length = k
        else
            A_number_of_columns = 2*m
            A_row_offset = k/4
            A_column_offset = m/2
            A_column_slice_length = m
        end if
        write (test_name, "(A56, A, A3, A, A2, I3, A, I3, A, I3, A9, I3, A3, I3, A8, I3, A)") &
                "gemm_comparing_blas::dgemm_complex128_slice_with_skips('", transa, "', '", transb, "', ", &
                m, ", ", n, ", ", k, ", alpha, A", lda, ", B, ", ldb, ", beta, C, ", ldc, ')'
        call initialize_random_matrix_complex128(A, transa, A_number_of_rows, 2*m, 2*k)
        call initialize_random_matrix_complex128(B, transb, ldb, k, n)
        allocate(matmul_C(ldc, n), ntcl_C(ldc, n))
        call random_complex128_matrix(matmul_C)
        ntcl_C = matmul_C
        call random_complex128_number(alpha)
        call random_complex128_number(beta)
        call ntcl_gemm(transa, transb, m, n, k, alpha, &
                        A(A_row_offset:(A_row_offset + 2*lda - 1):2, &
                          A_column_offset:(A_column_offset + A_column_slice_length - 1)), &
                          lda, B, ldb, beta, ntcl_C, ldc)
        allocate(matmul_A(lda, A_column_slice_length))
        matmul_A(:, :) = A(A_row_offset:(A_row_offset + 2*lda - 1):2, &
                     A_column_offset:(A_column_offset + A_column_slice_length - 1))
        matmul_C(1:m, 1:n) = &
            alpha*matmul(get_matrix_block(matmul_A, &
                                          m, k, transa /= 'n' .and. transa /= 'N'), &
                         get_matrix_block(B, k, n, transb /= 'n' .and. transb /= 'N')) &
                   +beta*matmul_C(1:m, 1:n)
        call assertion%is_equal(test_name, reshape(ntcl_C, (/m*n/)), reshape(matmul_C, (/m*n/)), tollerance)
        deallocate(A, B, matmul_C, ntcl_C)
    end subroutine test_random_matrix_slices_with_skips_complex128


    subroutine initialize_random_matrix_real32(matrix, trans, ld, m, n)
        real(real32), dimension(:, :), allocatable, intent(out) :: matrix
        character, intent(in) :: trans
        integer, intent(in) :: ld, m, n
        integer :: sd

        if (trans == 'n' .or. trans == 'N') then
            sd = n
        else
            sd = m
        end if
        allocate(matrix(1:ld, 1:sd))
        call random_number(matrix)
    end subroutine initialize_random_matrix_real32

    subroutine initialize_random_matrix_real64(matrix, trans, ld, m, n)
        real(real64), dimension(:, :), allocatable, intent(out) :: matrix
        character, intent(in) :: trans
        integer, intent(in) :: ld, m, n
        integer :: sd

        if (trans == 'n' .or. trans == 'N') then
            sd = n
        else
            sd = m
        end if
        allocate(matrix(1:ld, 1:sd))
        call random_number(matrix)
    end subroutine initialize_random_matrix_real64

    subroutine initialize_random_matrix_complex64(matrix, trans, ld, m, n)
        complex(real32), dimension(:, :), allocatable, intent(out) :: matrix
        character, intent(in) :: trans
        integer, intent(in) :: ld, m, n
        integer :: sd

        if (trans == 'n' .or. trans == 'N') then
            sd = n
        else
            sd = m
        end if
        allocate(matrix(1:ld, 1:sd))
        call random_complex64_matrix(matrix)
    end subroutine initialize_random_matrix_complex64

    subroutine initialize_random_matrix_complex128(matrix, trans, ld, m, n)
        complex(real64), dimension(:, :), allocatable, intent(out) :: matrix
        character, intent(in) :: trans
        integer, intent(in) :: ld, m, n
        integer :: sd

        if (trans == 'n' .or. trans == 'N') then
            sd = n
        else
            sd = m
        end if
        allocate(matrix(1:ld, 1:sd))
        call random_complex128_matrix(matrix)
    end subroutine initialize_random_matrix_complex128

    subroutine random_complex64_number(complex_number)
        complex(real32), intent(out) :: complex_number
        real(real32) :: real_part, imaginary_part

        call random_number(real_part)
        call random_number(imaginary_part)
        complex_number = cmplx(real_part, imaginary_part)
    end subroutine random_complex64_number

    subroutine random_complex64_matrix(matrix)
        complex(real32), dimension(:, :), intent(inout) :: matrix
        real(real32), dimension(size(matrix, 1), size(matrix, 2)) :: real_part, imaginary_part

        call random_number(real_part)
        call random_number(imaginary_part)
        matrix = cmplx(real_part, imaginary_part)
    end subroutine random_complex64_matrix

    subroutine random_complex128_number(complex_number)
        complex(real64), intent(out) :: complex_number
        real(real64) :: real_part, imaginary_part

        call random_number(real_part)
        call random_number(imaginary_part)
        complex_number = cmplx(real_part, imaginary_part)
    end subroutine random_complex128_number

    subroutine random_complex128_matrix(matrix)
        complex(real64), dimension(:, :), intent(inout) :: matrix
        real(real64), dimension(size(matrix, 1), size(matrix, 2)) :: real_part, imaginary_part

        call random_number(real_part)
        call random_number(imaginary_part)
        matrix = cmplx(real_part, imaginary_part)
    end subroutine random_complex128_matrix

    subroutine cleanup(this)
        class(gemm_comparing_with_blas_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(gemm_comparing_with_blas_test), intent(inout) :: this
    end subroutine clear

    function get_matrix_block_real32(matrix, &
                                     number_of_rows, &
                                     number_of_columns, &
                                     trans) result(transposed_matrix)
        real(real32), dimension(:,:), intent(in) :: matrix
        integer, intent(in) :: number_of_rows, number_of_columns
        logical, intent(in) :: trans

        real(real32), dimension(:,:), allocatable :: transposed_matrix
        if (trans) then
            transposed_matrix = transpose(matrix(1:number_of_columns, 1:number_of_rows))
        else
            transposed_matrix = matrix(1:number_of_rows, 1:number_of_columns)
        end if
    end function get_matrix_block_real32

    function get_matrix_block_real64(matrix, &
                                     number_of_rows, &
                                     number_of_columns, &
                                     trans) result(transposed_matrix)
        real(real64), dimension(:,:), intent(in) :: matrix
        integer, intent(in) :: number_of_rows, number_of_columns
        logical, intent(in) :: trans

        real(real64), dimension(:,:), allocatable :: transposed_matrix
        if (trans) then
            transposed_matrix = transpose(matrix(1:number_of_columns, 1:number_of_rows))
        else
            transposed_matrix = matrix(1:number_of_rows, 1:number_of_columns)
        end if
    end function get_matrix_block_real64

    function get_matrix_block_complex64(matrix, &
                                     number_of_rows, &
                                     number_of_columns, &
                                     trans) result(transposed_matrix)
        complex(real32), dimension(:,:), intent(in) :: matrix
        integer, intent(in) :: number_of_rows, number_of_columns
        logical, intent(in) :: trans

        complex(real32), dimension(:,:), allocatable :: transposed_matrix
        if (trans) then
            transposed_matrix = transpose(matrix(1:number_of_columns, 1:number_of_rows))
        else
            transposed_matrix = matrix(1:number_of_rows, 1:number_of_columns)
        end if
    end function get_matrix_block_complex64

    function get_matrix_block_complex128(matrix, &
                                     number_of_rows, &
                                     number_of_columns, &
                                     trans) result(transposed_matrix)
        complex(real64), dimension(:,:), intent(in) :: matrix
        integer, intent(in) :: number_of_rows, number_of_columns
        logical, intent(in) :: trans

        complex(real64), dimension(:,:), allocatable :: transposed_matrix
        if (trans) then
            transposed_matrix = transpose(matrix(1:number_of_columns, 1:number_of_rows))
        else
            transposed_matrix = matrix(1:number_of_rows, 1:number_of_columns)
        end if
    end function get_matrix_block_complex128
end module gemm_comparing_with_blas_test_module
