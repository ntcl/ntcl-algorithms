module repeated_calling_of_gemm_test_module
    use, intrinsic :: iso_fortran_env, only : real64

    use :: util_api, only : assert
    use :: gemm_interface_module, only : ntcl_gemm

    implicit none
    private

    public :: repeated_calling_of_gemm_test

    type :: repeated_calling_of_gemm_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type repeated_calling_of_gemm_test

    interface repeated_calling_of_gemm_test
        module procedure constructor
    end interface repeated_calling_of_gemm_test

    integer, parameter :: side = 10000
    integer, parameter :: number_of_iterations = 10
contains
    function constructor() result(this)
        type(repeated_calling_of_gemm_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(repeated_calling_of_gemm_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        real(real64), dimension(:, :), allocatable :: A, B, C

        integer :: iteration_index

        call assertion%equal("repeated_calling_of_gemm::Test complete", .true.)
        allocate(A(side, side), B(side, side), C(side, side))
        call random_number(A)
        call random_number(B)
        do iteration_index = 1, number_of_iterations 
            write (*,*) 'Iteration ', iteration_index, 'starts'
            if (mod(iteration_index, 2) == 1) then
                call ntcl_gemm('N', 'N', side, side, side, 1.0D0, A, side, B, side, 0.0D0, C, side)
                C = C/maxval(abs(C))
            else
                call ntcl_gemm('N', 'N', side, side, side, 1.0D0, C, side, B, side, 0.0D0, A, side)
                A = A/maxval(abs(A))
            end if
            write (*,*) 'Iteration ', iteration_index, 'ends'
        end do
        if (mod(number_of_iterations, 2) == 0) then
            write (*, *) 'sum: ', sum(A)
        else
            write (*, *) 'sum: ', sum(C)
        end if
        deallocate(A,B,C)
        call assertion%equal("repeated_calling_of_gemm::No memory leak found", .true.)
    end subroutine run

    subroutine cleanup(this)
        class(repeated_calling_of_gemm_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(repeated_calling_of_gemm_test), intent(inout) :: this
    end subroutine clear
end module repeated_calling_of_gemm_test_module
