module gemm_large_matrix_test_module
    use, intrinsic :: iso_fortran_env, only : real64, int64
    use :: util_api, only : assert

    use :: gemm_interface_module, only : ntcl_gemm

    implicit none
    private

    public :: gemm_large_matrix_test

    type :: gemm_large_matrix_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type gemm_large_matrix_test

    interface gemm_large_matrix_test
        module procedure constructor
    end interface gemm_large_matrix_test
contains
    function constructor() result(this)
        type(gemm_large_matrix_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(gemm_large_matrix_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        !integer, parameter :: side = 51234
        integer(int64), parameter :: desired_total_size = 60000000000_int64
        integer(int64) :: side = int(sqrt(real(desired_total_size/(3*8),kind=real64)), kind=int64) 
        real(real64), dimension(:, :), allocatable :: A, B, C
        integer(int64) :: total_size

        call assertion%equal("gemm_large_matrix::Test complete", .true.)
        allocate(A(side,side), B(side,side), C(side, side))
        total_size = side*side*8_int64 + side*side*8_int64 + side*side*8_int64 
        write (*,*) 'Size: ', total_size
        call assertion%equal("gemm_large_matrix::Total memeory is larger than 12GB", &
                                total_size > 12000000000_int64)

        call random_number(A)
        call random_number(B)
        call ntcl_gemm('N', 'N', int(side), int(side), int(side), 1.0D0, A, int(side), B, int(side), 0.0D0, C, int(side))

        call assertion%equal("gemm_large_matrix::No crash occured", .true.)

        deallocate(A, B, C)
    end subroutine run

    subroutine cleanup(this)
        class(gemm_large_matrix_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(gemm_large_matrix_test), intent(inout) :: this
    end subroutine clear
end module gemm_large_matrix_test_module
