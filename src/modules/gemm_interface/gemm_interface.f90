module gemm_interface_module
    use, intrinsic :: iso_fortran_env, only : real32, real64

    use :: util_api, only : &
                tile

    use :: tensor_api, only : &
                tensor, &
                scalar, &
                point_to_tensor, &
                release_tensor_pointer, &
                copy_tensor, &
                matrix_data_primitives_factory, &
                matrix_data_primitives, &
                secure_fortran_pointer_from_tensor, &
                release_pointer_from_remote_tensor

    use :: matrix_module, only : matrix

    use :: matrix_multiplication_api, only : &
                matrix_multiplication, &
                matrix_multiplication_factory

    implicit none
    private

    public :: ntcl_gemm_initialize
    public :: ntcl_gemm_finalize
    public :: ntcl_gemm
    public :: ntcl_gemm_complex128
    public :: ntcl_gemm_complex64
    public :: ntcl_gemm_real64
    public :: ntcl_gemm_real32

    interface ntcl_gemm
        module procedure ntcl_gemm_complex128
        module procedure ntcl_gemm_complex64
        module procedure ntcl_gemm_real64
        module procedure ntcl_gemm_real32
    end interface

    interface setup_matrix
        module procedure setup_matrix_complex128
        module procedure setup_matrix_complex64
        module procedure setup_matrix_real64
        module procedure setup_matrix_real32
    end interface setup_matrix

    interface initialize_from_old_style_fortran_matrix
        module procedure initialize_from_oldstyle_fortran_matrix_complex128
        module procedure initialize_from_oldstyle_fortran_matrix_complex64
        module procedure initialize_from_oldstyle_fortran_matrix_real64
        module procedure initialize_from_oldstyle_fortran_matrix_real32
    end interface initialize_from_old_style_fortran_matrix

    interface save_to_old_style_fortran_matrix
        module procedure save_to_old_style_fortran_matrix_complex128
        module procedure save_to_old_style_fortran_matrix_complex64
        module procedure save_to_old_style_fortran_matrix_real64
        module procedure save_to_old_style_fortran_matrix_real32
    end interface save_to_old_style_fortran_matrix

    class(matrix_multiplication), allocatable :: mm
    type(matrix_data_primitives) :: mdp
contains

    subroutine ntcl_gemm_initialize()
        mm = matrix_multiplication_factory%get()
        mdp = matrix_data_primitives_factory%get()
    end subroutine ntcl_gemm_initialize

    subroutine ntcl_gemm_finalize()
        call mdp%cleanup()
        call mm%cleanup()
    end subroutine ntcl_gemm_finalize

    subroutine ntcl_gemm_complex128(transa, transb, m, n, k, alpha, &
                         A, lda, B, ldb, beta, C, ldc)
        character, intent(in) :: transa, transb
        integer, intent(in) :: m, n, k, lda, ldb, ldc
        complex(real64), intent(in) :: alpha, beta
        complex(real64), dimension(ldc, *), target, intent(inout) :: C
        complex(real64), dimension(lda, *), target, intent(in) :: A
        complex(real64), dimension(ldb, *), target, intent(in) :: B

        type(matrix) :: matrix_A, matrix_B, matrix_C

        matrix_A = setup_matrix(lda, m, k, is_transposed(transa), A)
        matrix_B = setup_matrix(ldb, k, n, is_transposed(transb), B)
        matrix_C = setup_matrix(ldc, m, n, .false., C)
        call multiply(matrix_C, matrix_A, matrix_B, &
                      scalar(alpha), scalar(beta))
        call save_to_old_style_fortran_matrix(ldc, m, n, C, matrix_C)
        call matrix_A%cleanup()
        call matrix_B%cleanup()
        call matrix_C%cleanup()
    end subroutine ntcl_gemm_complex128

    subroutine ntcl_gemm_complex64(transa, transb, m, n, k, alpha, &
                         A, lda, B, ldb, beta, C, ldc)
        character, intent(in) :: transa, transb
        integer, intent(in) :: m, n, k, lda, ldb, ldc
        complex(real32), intent(in) :: alpha, beta
        complex(real32), dimension(ldc, *), target, intent(inout) :: C
        complex(real32), dimension(lda, *), target, intent(in) :: A
        complex(real32), dimension(ldb, *), target, intent(in) :: B

        type(matrix) :: matrix_A, matrix_B, matrix_C

        matrix_A = setup_matrix(lda, m, k, is_transposed(transa), A)
        matrix_B = setup_matrix(ldb, k, n, is_transposed(transb), B)
        matrix_C = setup_matrix(ldc, m, n, .false., C)
        call multiply(matrix_C, matrix_A, matrix_B, &
                      scalar(alpha), scalar(beta))
        call save_to_old_style_fortran_matrix(ldc, m, n, C, matrix_C)
        call matrix_A%cleanup()
        call matrix_B%cleanup()
        call matrix_C%cleanup()
    end subroutine ntcl_gemm_complex64

    subroutine ntcl_gemm_real64(transa, transb, m, n, k, alpha, &
                         A, lda, B, ldb, beta, C, ldc)
        character, intent(in) :: transa, transb
        integer, intent(in) :: m, n, k, lda, ldb, ldc
        real(real64), intent(in) :: alpha, beta
        real(real64), dimension(ldc, *), target, intent(inout) :: C
        real(real64), dimension(lda, *), target, intent(in) :: A
        real(real64), dimension(ldb, *), target, intent(in) :: B

        type(matrix) :: matrix_A, matrix_B, matrix_C

        matrix_A = setup_matrix(lda, m, k, is_transposed(transa), A)
        matrix_B = setup_matrix(ldb, k, n, is_transposed(transb), B)
        matrix_C = setup_matrix(ldc, m, n, .false., C)
        call multiply(matrix_C, matrix_A, matrix_B, &
                      scalar(alpha), scalar(beta))
        call save_to_old_style_fortran_matrix(ldc, m, n, C, matrix_C)
        call matrix_A%cleanup()
        call matrix_B%cleanup()
        call matrix_C%cleanup()
    end subroutine ntcl_gemm_real64

    subroutine ntcl_gemm_real32(transa, transb, m, n, k, alpha, &
                         A, lda, B, ldb, beta, C, ldc)
        character, intent(in) :: transa, transb
        integer, intent(in) :: m, n, k, lda, ldb, ldc
        real(real32), intent(in) :: alpha, beta
        real(real32), dimension(ldc, *), target, intent(inout) :: C
        real(real32), dimension(lda, *), target, intent(in) :: A
        real(real32), dimension(ldb, *), target, intent(in) :: B

        type(matrix) :: matrix_A, matrix_B, matrix_C

        matrix_A = setup_matrix(lda, m, k, is_transposed(transa), A)
        matrix_B = setup_matrix(ldb, k, n, is_transposed(transb), B)
        matrix_C = setup_matrix(ldc, m, n, .false., C)
        call multiply(matrix_C, matrix_A, matrix_B, &
                      scalar(alpha), scalar(beta))
        call save_to_old_style_fortran_matrix(ldc, m, n, C, matrix_C)
        call matrix_A%cleanup()
        call matrix_B%cleanup()
        call matrix_C%cleanup()
    end subroutine ntcl_gemm_real32

    subroutine multiply(result_matrix, &
                        left_matrix, &
                        right_matrix, &
                        alpha, &
                        beta)
        type(matrix), intent(inout) :: result_matrix, left_matrix, right_matrix
        type(scalar), intent(in) :: alpha, beta


        call mm%mm(result_matrix, left_matrix, right_matrix, &
                   alpha = alpha, beta = beta)
    end subroutine multiply

    function setup_matrix_complex128(leading_dimension, &
                                     number_of_rows, &
                                     number_of_columns, &
                                     matrix_is_transposed, &
                                     oldstyle_fortran_matrix) result(result_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        complex(real64), dimension(leading_dimension, *), target, intent(in) :: &
                    oldstyle_fortran_matrix

        type(matrix) :: result_matrix
        type(matrix) :: full_matrix


        full_matrix = matrix()
        call initialize_from_old_style_fortran_matrix(full_matrix, &
                                                      leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      matrix_is_transposed, &
                                                      oldstyle_fortran_matrix)
        result_matrix = get_active_matrix(full_matrix, &
                                          leading_dimension, &
                                          number_of_rows, &
                                          number_of_columns, &
                                          matrix_is_transposed)
        call release_tensor_pointer(full_matrix)
        result_matrix%is_transposed = matrix_is_transposed
    end function setup_matrix_complex128

    function setup_matrix_complex64(leading_dimension, &
                                 number_of_rows, &
                                 number_of_columns, &
                                 matrix_is_transposed, &
                                 oldstyle_fortran_matrix) result(result_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        complex(real32), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        type(matrix) :: result_matrix
        type(matrix) :: full_matrix


        full_matrix = matrix()
        call initialize_from_old_style_fortran_matrix(full_matrix, &
                                                      leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      matrix_is_transposed, &
                                                      oldstyle_fortran_matrix)
        result_matrix = get_active_matrix(full_matrix, &
                                          leading_dimension, &
                                          number_of_rows, &
                                          number_of_columns, &
                                          matrix_is_transposed)
        call release_tensor_pointer(full_matrix)
        result_matrix%is_transposed = matrix_is_transposed
    end function setup_matrix_complex64

    function setup_matrix_real64(leading_dimension, &
                                 number_of_rows, &
                                 number_of_columns, &
                                 matrix_is_transposed, &
                                 oldstyle_fortran_matrix) result(result_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        real(real64), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        type(matrix) :: result_matrix
        type(matrix) :: full_matrix

        full_matrix = matrix()
        call initialize_from_old_style_fortran_matrix(full_matrix, &
                                                      leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      matrix_is_transposed, &
                                                      oldstyle_fortran_matrix)
        result_matrix = get_active_matrix(full_matrix, &
                                          leading_dimension, &
                                          number_of_rows, &
                                          number_of_columns, &
                                          matrix_is_transposed)
        call release_tensor_pointer(full_matrix)
        result_matrix%is_transposed = matrix_is_transposed
    end function setup_matrix_real64

    function setup_matrix_real32(leading_dimension, &
                                 number_of_rows, &
                                 number_of_columns, &
                                 matrix_is_transposed, &
                                 oldstyle_fortran_matrix) result(result_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        real(real32), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        type(matrix) :: result_matrix
        type(matrix) :: full_matrix

        full_matrix = matrix()
        call initialize_from_old_style_fortran_matrix(full_matrix, &
                                                      leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      matrix_is_transposed, &
                                                      oldstyle_fortran_matrix)
        result_matrix = get_active_matrix(full_matrix, &
                                          leading_dimension, &
                                          number_of_rows, &
                                          number_of_columns, &
                                          matrix_is_transposed)
        call release_tensor_pointer(full_matrix)
        result_matrix%is_transposed = matrix_is_transposed
    end function setup_matrix_real32

    subroutine initialize_from_oldstyle_fortran_matrix_complex128(result_matrix, &
                                                                  leading_dimension, &
                                                                  number_of_rows, &
                                                                  number_of_columns, &
                                                                  matrix_is_transposed, &
                                                                  oldstyle_fortran_matrix)
        type(matrix), intent(inout) :: result_matrix
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        complex(real64), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        complex(real64), dimension(:, :), pointer :: pointer_to_matrix
        integer :: secondary_dimension

        if (matrix_is_transposed) then
            secondary_dimension = number_of_rows
        else
            secondary_dimension = number_of_columns
        end if
        pointer_to_matrix => oldstyle_fortran_matrix(1:leading_dimension, 1:secondary_dimension)
        call point_to_tensor(result_matrix, pointer_to_matrix, memory_type='host')
    end subroutine initialize_from_oldstyle_fortran_matrix_complex128

    subroutine initialize_from_oldstyle_fortran_matrix_complex64(result_matrix, &
                                                                 leading_dimension, &
                                                                 number_of_rows, &
                                                                 number_of_columns, &
                                                                 matrix_is_transposed, &
                                                                 oldstyle_fortran_matrix)
        type(matrix), intent(inout) :: result_matrix
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        complex(real32), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        complex(real32), dimension(:, :), pointer :: pointer_to_matrix
        integer :: secondary_dimension

        if (matrix_is_transposed) then
            secondary_dimension = number_of_rows
        else
            secondary_dimension = number_of_columns
        end if
        pointer_to_matrix => oldstyle_fortran_matrix(1:leading_dimension, 1:secondary_dimension)
        call point_to_tensor(result_matrix, pointer_to_matrix, memory_type='host')
    end subroutine initialize_from_oldstyle_fortran_matrix_complex64

    subroutine initialize_from_oldstyle_fortran_matrix_real64(result_matrix, &
                                                              leading_dimension, &
                                                              number_of_rows, &
                                                              number_of_columns, &
                                                              matrix_is_transposed, &
                                                              oldstyle_fortran_matrix)
        type(matrix), intent(inout) :: result_matrix
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        real(real64), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        real(real64), dimension(:, :), pointer :: pointer_to_matrix
        integer :: secondary_dimension

        if (matrix_is_transposed) then
            secondary_dimension = number_of_rows
        else
            secondary_dimension = number_of_columns
        end if
        pointer_to_matrix => oldstyle_fortran_matrix(1:leading_dimension, 1:secondary_dimension)
        call point_to_tensor(result_matrix, pointer_to_matrix, memory_type='host')
    end subroutine initialize_from_oldstyle_fortran_matrix_real64

    subroutine initialize_from_oldstyle_fortran_matrix_real32(result_matrix, &
                                                              leading_dimension, &
                                                              number_of_rows, &
                                                              number_of_columns, &
                                                              matrix_is_transposed, &
                                                              oldstyle_fortran_matrix)
        type(matrix), intent(inout) :: result_matrix
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed
        real(real32), dimension(leading_dimension, *), target, intent(in) :: &
                oldstyle_fortran_matrix

        real(real32), dimension(:, :), pointer :: pointer_to_matrix
        integer :: secondary_dimension

        if (matrix_is_transposed) then
            secondary_dimension = number_of_rows
        else
            secondary_dimension = number_of_columns
        end if
        pointer_to_matrix => oldstyle_fortran_matrix(1:leading_dimension, 1:secondary_dimension)
        call point_to_tensor(result_matrix, pointer_to_matrix, memory_type='host')
    end subroutine initialize_from_oldstyle_fortran_matrix_real32

    subroutine save_to_old_style_fortran_matrix_complex128(leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      fortran_matrix, &
                                                      ntcl_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        complex(real64), dimension(leading_dimension, *), intent(inout) :: fortran_matrix
        type(matrix), intent(in) :: ntcl_matrix

        complex(real64), dimension(:,:), pointer, contiguous :: contiguous_matrix_pointer

        call secure_fortran_pointer_from_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
        fortran_matrix(1:number_of_rows, 1:number_of_columns) = &
             contiguous_matrix_pointer(1:number_of_rows, 1:number_of_columns)
        call release_pointer_from_remote_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
    end subroutine save_to_old_style_fortran_matrix_complex128

    subroutine save_to_old_style_fortran_matrix_complex64(leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      fortran_matrix, &
                                                      ntcl_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        complex(real32), dimension(leading_dimension, *), intent(inout) :: fortran_matrix
        type(matrix), intent(in) :: ntcl_matrix

        complex(real32), dimension(:,:), pointer, contiguous :: contiguous_matrix_pointer

        call secure_fortran_pointer_from_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
        fortran_matrix(1:number_of_rows, 1:number_of_columns) = &
             contiguous_matrix_pointer(1:number_of_rows, 1:number_of_columns)
        call release_pointer_from_remote_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
    end subroutine save_to_old_style_fortran_matrix_complex64

    subroutine save_to_old_style_fortran_matrix_real64(leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      fortran_matrix, &
                                                      ntcl_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        real(real64), dimension(leading_dimension, *), intent(inout) :: fortran_matrix
        type(matrix), intent(in) :: ntcl_matrix

        real(real64), dimension(:,:), pointer, contiguous :: contiguous_matrix_pointer

        call secure_fortran_pointer_from_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
        fortran_matrix(1:number_of_rows, 1:number_of_columns) = &
             contiguous_matrix_pointer(1:number_of_rows, 1:number_of_columns)
        call release_pointer_from_remote_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
    end subroutine save_to_old_style_fortran_matrix_real64

    subroutine save_to_old_style_fortran_matrix_real32(leading_dimension, &
                                                      number_of_rows, &
                                                      number_of_columns, &
                                                      fortran_matrix, &
                                                      ntcl_matrix)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        real(real32), dimension(leading_dimension, *), intent(inout) :: fortran_matrix
        type(matrix), intent(in) :: ntcl_matrix

        real(real32), dimension(:,:), pointer, contiguous :: contiguous_matrix_pointer

        call secure_fortran_pointer_from_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
        fortran_matrix(1:number_of_rows, 1:number_of_columns) = &
            contiguous_matrix_pointer(1:number_of_rows, 1:number_of_columns)
        call release_pointer_from_remote_tensor(contiguous_matrix_pointer, &
                                                ntcl_matrix)
    end subroutine save_to_old_style_fortran_matrix_real32

    function get_active_matrix(full_matrix, &
                               leading_dimension, &
                               number_of_rows, &
                               number_of_columns, &
                               matrix_is_transposed) result(active_matrix)
        type(matrix), intent(in) :: full_matrix
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed

        type(matrix) :: active_matrix


        if (must_copy(leading_dimension, &
                      number_of_rows, &
                      number_of_columns, &
                      matrix_is_transposed)) then
            call mdp%allocate_and_copy_tile(active_matrix, &
                                            full_matrix, &
                                            setup_matrix_tile(number_of_rows, &
                                                              number_of_columns, &
                                                              matrix_is_transposed))
        else
            call mdp%allocate_and_copy_tile(active_matrix, &
                                            full_matrix, &
                                            setup_matrix_tile(number_of_rows, &
                                                              number_of_columns, &
                                                              matrix_is_transposed))
        end if
    end function get_active_matrix

    function setup_matrix_tile(number_of_rows, &
                               number_of_columns, &
                               matrix_is_transposed) result(matrix_tile)
        integer, intent(in) :: number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed

        type(tile) :: matrix_tile
        if (matrix_is_transposed) then
            matrix_tile = tile(1, number_of_columns, 1, number_of_rows)
        else
            matrix_tile = tile(1, number_of_rows, 1, number_of_columns)
        end if
    end function setup_matrix_tile

    pure logical function must_copy(leading_dimension, &
                               number_of_rows, &
                               number_of_columns, &
                               matrix_is_transposed)
        integer, intent(in) :: leading_dimension, number_of_rows, number_of_columns
        logical, intent(in) :: matrix_is_transposed

        if (matrix_is_transposed) then
            must_copy = leading_dimension > number_of_columns
        else
            must_copy = leading_dimension > number_of_rows
        end if
    end function must_copy

    pure logical function is_transposed(trans)
        character, intent(in) :: trans
        is_transposed = trans == 'T' .or. trans == 't' .or. trans == 'C' .or. trans == 'c'
    end function is_transposed
end module gemm_interface_module
