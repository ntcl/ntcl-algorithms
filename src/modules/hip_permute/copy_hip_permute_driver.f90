module copy_hip_permute_driver_module
    use, intrinsic :: iso_fortran_env, only : &
            int64, &
            real32, &
            real64

    use iso_c_binding, only : &
            c_int, &
            c_float, &
            c_double, &
            c_int64_t, &
            c_ptr, &
            c_null_ptr, &
            c_loc

    use :: data_api, only : &
            stream

    use :: tensor_api, only : &
            tensor, &
            vector, &
            dt_r32, &
            dt_r64, &
            dt_c64, &
            dt_c128

    use :: tensor_permute_dev, only : &
            raw_tensor_permute

    use :: async_hip_permute_driver_module, only : async_hip_permute_driver

    use :: hip_permute_parameters_module, only : &
            hip_permute_parameters, &
            hip_permute_default_elements_per_thread, &
            hip_permute_default_threads_per_block

    implicit none
    private

    public :: copy_hip_permute_driver

    type, extends(raw_tensor_permute) :: copy_hip_permute_driver
        type(async_hip_permute_driver) :: driver
        integer :: elements_per_thread
        integer :: threads_per_block
    contains
        procedure :: permute_tensor => permute_tensor
        procedure :: get_parameters => get_parameters
        procedure :: is_copy => is_copy
        procedure :: set_elements_per_thread => set_elements_per_thread
        procedure :: set_threads_per_block => set_threads_per_block
        procedure :: synchronize => synchronize
        procedure :: cleanup => cleanup

        procedure :: copy => copy
    end type copy_hip_permute_driver

    interface copy_hip_permute_driver
        module procedure constructor_empty
        module procedure constructor
    end interface copy_hip_permute_driver

    interface
        subroutine hip_tensor_copy_real32_execute(d_src, d_dst, params, astream) &
                    bind(C, name="hip_tensor_copy_real32_execute")
            import :: c_ptr
            import :: hip_permute_parameters

            type(c_ptr), value, intent(in) :: d_src, d_dst
            type(hip_permute_parameters) :: params
            type(c_ptr), value, intent(in) :: astream
        end subroutine hip_tensor_copy_real32_execute

        subroutine hip_tensor_copy_real64_execute(d_src, d_dst, params, astream) &
                    bind(C, name="hip_tensor_copy_real64_execute")
            import :: c_ptr
            import :: hip_permute_parameters

            type(c_ptr), value, intent(in) :: d_src, d_dst
            type(hip_permute_parameters) :: params
            type(c_ptr), value, intent(in) :: astream
        end subroutine hip_tensor_copy_real64_execute

        subroutine hip_tensor_copy_complex64_execute(d_src, d_dst, params, astream) &
                    bind(C, name="hip_tensor_copy_complex64_execute")
            import :: c_ptr
            import :: hip_permute_parameters

            type(c_ptr), value, intent(in) :: d_src, d_dst
            type(hip_permute_parameters) :: params
            type(c_ptr), value, intent(in) :: astream
        end subroutine hip_tensor_copy_complex64_execute

        subroutine hip_tensor_copy_complex128_execute(d_src, d_dst, params, astream) &
                    bind(C, name="hip_tensor_copy_complex128_execute")
            import :: c_ptr
            import :: hip_permute_parameters

            type(c_ptr), value, intent(in) :: d_src, d_dst
            type(hip_permute_parameters) :: params
            type(c_ptr), value, intent(in) :: astream
        end subroutine hip_tensor_copy_complex128_execute
    end interface
contains
    function constructor_empty() result(this)
        type(copy_hip_permute_driver) :: this

        this%elements_per_thread = hip_permute_default_elements_per_thread
        this%threads_per_block = hip_permute_default_threads_per_block
    end function constructor_empty

    function constructor(driver) result(this)
        type(async_hip_permute_driver), intent(in) :: driver
        type(copy_hip_permute_driver) :: this

        this = copy_hip_permute_driver()
        this%driver = driver
    end function constructor

    subroutine permute_tensor(this, src, dst, perm, astream)
        class(copy_hip_permute_driver), intent(inout) :: this
        class(tensor), intent(in) :: src
        class(tensor), intent(inout) :: dst
        integer, dimension(:), intent(in) :: perm
        type(stream), intent(in), optional :: astream

        if ( this%is_copy(perm)) then
            call this%copy(src, dst, perm, astream)
        else
            call this%driver%permute_tensor(src, dst, perm, astream)
        end if
    end subroutine permute_tensor
        
    subroutine copy(this, src, dst, perm, astream)
        class(copy_hip_permute_driver), intent(inout) :: this
        class(tensor), intent(in) :: src
        class(tensor), intent(inout) :: dst
        integer, dimension(:), intent(in) :: perm
        type(stream), intent(in), optional :: astream

        type(c_ptr) :: src_device_cptr, dst_device_cptr
        type(c_ptr) :: actual_stream
        type(hip_permute_parameters) :: parameters

        if ( .not. this%is_compatible(src, dst, perm) ) &
                error stop "copy_hip_permute_driver::copy:Tensors are not compatible."

        ! get c pointers to src and dst on device
        call this%driver%converter%secure_pointer(src, src_device_cptr, astream)
        call this%driver%converter%secure_pointer(dst, dst_device_cptr, astream)

        parameters = this%get_parameters(src)

        ! TODO: Use default stream?
        actual_stream = c_null_ptr
        if ( present(astream) ) actual_stream = astream%sid

        select case (src%get_datatype())
        case ( dt_r32 )
            call hip_tensor_copy_real32_execute( &
                    src_device_cptr, dst_device_cptr, parameters, actual_stream)
        case ( dt_r64 )
            call hip_tensor_copy_real64_execute( &
                    src_device_cptr, dst_device_cptr, parameters, actual_stream)
        case ( dt_c64 )
            call hip_tensor_copy_complex64_execute( &
                    src_device_cptr, dst_device_cptr, parameters, actual_stream)
        case ( dt_c128 )
            call hip_tensor_copy_complex128_execute( &
                    src_device_cptr, dst_device_cptr, parameters, actual_stream)
        case default
            error stop "copy_hip_permute_driver::copy:Datatype not supported."
        end select

        call this%driver%converter%update_and_release(dst, dst_device_cptr, astream)
        call this%driver%converter%release(src, src_device_cptr, astream)
    end subroutine copy

    type(hip_permute_parameters) function get_parameters(this, src)
        class(copy_hip_permute_driver), intent(in) :: this
        class(tensor), intent(in), target :: src

        get_parameters%rank = src%get_rank()
        get_parameters%number_of_elements = src%number_of_elements
        get_parameters%dimensions = c_loc(src%dims)
        get_parameters%elements_per_thread = this%elements_per_thread
        get_parameters%threads_per_block = this%threads_per_block
    end function get_parameters

    logical function is_copy(this, perm)
        class(copy_hip_permute_driver), intent(in) :: this
        integer, dimension(:), intent(in) :: perm

        integer :: idx

        is_copy = all(perm == [(idx, idx=1, size(perm))])
    end function is_copy

    subroutine set_elements_per_thread(this, elements_per_thread)
        class(copy_hip_permute_driver), intent(inout) :: this
        integer, intent(in) :: elements_per_thread

        this%elements_per_thread = elements_per_thread
    end subroutine set_elements_per_thread

    subroutine set_threads_per_block(this, threads_per_block)
        class(copy_hip_permute_driver), intent(inout) :: this
        integer, intent(in) :: threads_per_block

        this%threads_per_block = threads_per_block
    end subroutine set_threads_per_block

    subroutine synchronize(this, astream)
        class(copy_hip_permute_driver), intent(in) :: this
        type(stream), intent(in), optional :: astream

        call this%driver%synchronize(astream)
    end subroutine synchronize

    subroutine cleanup(this)
        class(copy_hip_permute_driver), intent(inout) :: this

        call this%driver%cleanup()
    end subroutine cleanup
end module copy_hip_permute_driver_module
