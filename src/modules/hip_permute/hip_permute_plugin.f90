module hip_permute_plugin
    use :: tensor_permute_hip_module, only : tensor_permute_hip
    use :: async_hip_permute_driver_module, only : async_hip_permute_driver
    use :: tailored_hip_permute_driver_module, only : tailored_hip_permute_driver
    use :: copy_hip_permute_driver_module, only : copy_hip_permute_driver
    use :: hip_permute_parameters_module, only : &
            hip_permute_default_elements_per_thread, &
            hip_permute_default_threads_per_block

    implicit none
    public
end module hip_permute_plugin
