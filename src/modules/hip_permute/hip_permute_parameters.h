#pragma once

typedef struct {
  int rank;
  size_t number_of_elements;
  size_t * dimensions;
  int elements_per_thread;
  int threads_per_block;
} hip_permute_parameters;
