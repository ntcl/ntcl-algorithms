module hip_permute_parameters_module
    use, intrinsic :: iso_c_binding, only : &
            c_int, &
            c_size_t, &
            c_ptr

    implicit none
    private

    public :: hip_permute_parameters
    public :: hip_permute_default_elements_per_thread
    public :: hip_permute_default_threads_per_block

    type, bind(c) :: hip_permute_parameters
        integer(c_int) :: rank
        integer(c_size_t) :: number_of_elements
        type(c_ptr) :: dimensions
        integer(c_int) :: elements_per_thread
        integer(c_int) :: threads_per_block
    end type hip_permute_parameters

    integer, parameter :: hip_permute_default_elements_per_thread = 4
    integer, parameter :: hip_permute_default_threads_per_block = 256
end module hip_permute_parameters_module
