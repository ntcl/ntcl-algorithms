module matrix_multiplication_api
    use :: util_api, only : &
            dictionary

    use :: matrix_multiplication_module, only : matrix_multiplication
    use :: mm_factory_module, only : mm_factory

    implicit none
    public

    class(mm_factory), allocatable :: matrix_multiplication_factory
contains
    subroutine mm_initialize(factory, options)
        class(mm_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(matrix_multiplication_factory) ) &
                error stop 'mm_initialize::Already initialized.'

        matrix_multiplication_factory = factory

        if (present(options) ) &
                call matrix_multiplication_factory%set_default_options(options)
    end subroutine mm_initialize

    subroutine mm_finalize()

        if ( allocated(matrix_multiplication_factory) ) then
            call matrix_multiplication_factory%cleanup()
            deallocate(matrix_multiplication_factory)
        end if
    end subroutine mm_finalize
end module matrix_multiplication_api
