module tensor_permute_api
    use :: util_api, only : &
            dictionary

    use :: tensor_permute_module, only : tensor_permute
    use :: permute_factory_module, only : permute_factory

    implicit none

    class(permute_factory), allocatable :: tensor_permute_factory
contains
    subroutine tp_initialize(factory, options)
        class(permute_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(tensor_permute_factory)) &
                error stop 'tp_initialize::Already initialized.'

        tensor_permute_factory = factory
        if ( present(options) ) call tensor_permute_factory%set_default_options(options)
    end subroutine tp_initialize

    subroutine tp_finalize()

        if (allocated(tensor_permute_factory)) then
            call tensor_permute_factory%cleanup()
            deallocate(tensor_permute_factory)
        end if
    end subroutine tp_finalize
end module tensor_permute_api
