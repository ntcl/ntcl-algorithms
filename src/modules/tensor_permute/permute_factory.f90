module permute_factory_module
    use :: util_api, only : &
            string, &
            dictionary, &
            dictionary_converter

    use :: tensor_api, only : tensor
    use :: tensor_permute_module, only : tensor_permute

    implicit none
    private

    public :: permute_factory

    type, abstract :: permute_factory
        type(dictionary) :: default_options
    contains
        procedure :: get => get
        procedure :: get_best_driver => get_best_driver
        procedure :: create => create
        procedure :: create_best_driver => create_best_driver
        procedure :: set_default_options => set_default_options
        procedure :: cleanup_permute_factory => cleanup_permute_factory
        procedure(create_interface), deferred :: create_from_key
        procedure(get_available_interface), deferred :: get_available_permute_drivers
        procedure(build_interface), deferred :: build
        procedure(get_key_from_tensor_interface), deferred :: get_key_from_tensor
        procedure(empty), deferred :: cleanup
    end type permute_factory

    abstract interface
        subroutine create_interface(this, permute, key)
            import :: permute_factory
            import :: tensor_permute
            import :: string

            class(permute_factory), intent(in) :: this
            class(tensor_permute), allocatable, intent(inout) :: permute
            type(string), intent(in) :: key
        end subroutine create_interface

        subroutine build_interface(this, driver, options, priorities)
            import :: permute_factory
            import :: tensor_permute
            import :: dictionary
            import :: string

            class(permute_factory), intent(in) :: this
            class(tensor_permute), intent(inout) :: driver
            type(dictionary), intent(in), optional :: options
            type(string), dimension(:), intent(in), optional :: priorities
        end subroutine build_interface

        function get_available_interface(this) result(drivers)
            import :: permute_factory
            import :: string

            class(permute_factory), intent(in) :: this
            type(string), dimension(:), allocatable :: drivers
        end function get_available_interface

        function get_key_from_tensor_interface(this, atensor) result(key)
            import :: permute_factory
            import :: tensor
            import :: string

            class(permute_factory), intent(in) :: this
            class(tensor), intent(in) :: atensor
            type(string) :: key
        end function get_key_from_tensor_interface

        subroutine empty(this)
            import :: permute_factory

            class(permute_factory), intent(inout) :: this
        end subroutine empty
    end interface

    character(len=*), parameter :: permute_driver_key = "permute_driver"
    character(len=*), parameter :: default_permute_driver = "default"
contains
    function get(this, driver, options, priorities) result(permute)
        class(permute_factory), intent(in) :: this
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        class(tensor_permute), allocatable :: permute

        call this%create(permute, driver, options, priorities)
    end function get

    function get_best_driver(this, atensor) result(permute)
        class(permute_factory), intent(in) :: this
        class(tensor), intent(in) :: atensor
        class(tensor_permute), allocatable :: permute

        call this%create_best_driver(permute, atensor)
    end function get_best_driver

    subroutine create(this, permute, driver, options, priorities)
        class(permute_factory), intent(in) :: this
        class(tensor_permute), allocatable, intent(inout) :: permute
        character(len=*), intent(in), optional :: driver
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities

        type(string) :: key
        type(dictionary_converter) :: conv

        if ( present(driver) ) then
            key = driver
        else
            key = conv%to_string_with_defaults(permute_driver_key, this%default_options, &
                    options, priorities, default_permute_driver)
        end if
        call this%create_from_key(permute, key)
        call this%build(permute, options, priorities)
    end subroutine create

    subroutine create_best_driver(this, permute, atensor)
        class(permute_factory), intent(in) :: this
        class(tensor_permute), allocatable, intent(inout) :: permute
        class(tensor), intent(in) :: atensor

        call this%create_from_key(permute, this%get_key_from_tensor(atensor))
        call this%build(permute)
    end subroutine create_best_driver

    subroutine set_default_options(this, options)
        class(permute_factory), intent(inout) :: this
        type(dictionary), intent(in) :: options

        this%default_options = options
    end subroutine set_default_options

    subroutine cleanup_permute_factory(this)
        class(permute_factory), intent(inout) :: this

        call this%default_options%cleanup()
    end subroutine cleanup_permute_factory

end module permute_factory_module
