module tensor_contraction_api
    use :: util_api, only : &
            dictionary

    use :: tensor_contraction_module, only : tensor_contraction
    use :: contraction_factory_module, only : contraction_factory

    implicit none
    private

    public :: tensor_contraction
    public :: tensor_contraction_factory
    public :: tc_initialize
    public :: tc_finalize

    class(contraction_factory), allocatable :: tensor_contraction_factory
contains
    subroutine tc_initialize(factory, options)
        class(contraction_factory), intent(in) :: factory
        type(dictionary), intent(in), optional :: options

        if ( allocated(tensor_contraction_factory) ) &
                error stop 'tc_initialize::Already initialized'

        tensor_contraction_factory = factory
        if ( present(options) ) &
                call tensor_contraction_factory%set_default_options(options)
    end subroutine tc_initialize

    subroutine tc_finalize()

        if (allocated(tensor_contraction_factory)) then
            call tensor_contraction_factory%cleanup()
            deallocate(tensor_contraction_factory)
        end if
    end subroutine tc_finalize
end module tensor_contraction_api
