module global_buffered_ttgt_tc_driver_module
    use, intrinsic :: iso_fortran_env, only : int64
    use :: data_api, only : &
            stream, &
            global_scratch_buffer

    use :: tensor_api, only : &
            tensor, &
            scalar

    use :: tc_ttgt_module, only : tc_ttgt
    use :: ttgt_descriptor_module, only : ttgt_descriptor
    use :: tensor_permute_module, only : tensor_permute
    use :: matrix_multiplication_module, only : matrix_multiplication

    implicit none
    private

    public :: global_buffered_ttgt_tc_driver

    type, extends(tc_ttgt) :: global_buffered_ttgt_tc_driver
    contains
        procedure :: contract => contract
        procedure :: synchronize => synchronize
    end type global_buffered_ttgt_tc_driver

    interface global_buffered_ttgt_tc_driver
        module procedure constructor_empty
        module procedure constructor
    end interface global_buffered_ttgt_tc_driver
contains
    function constructor_empty() result(this)
        type(global_buffered_ttgt_tc_driver) :: this

        call this%clear()
    end function constructor_empty

    function constructor(descr, permuter, mm) result(this)
        type(ttgt_descriptor), intent(in) :: descr
        class(tensor_permute), intent(in) :: permuter
        class(matrix_multiplication), intent(in) :: mm
        type(global_buffered_ttgt_tc_driver) :: this

        this = global_buffered_ttgt_tc_driver()

        call this%setup(descr, permuter, mm)
    end function constructor

    subroutine contract(this, c, a, b, alpha, beta, astream)
        class(global_buffered_ttgt_tc_driver), intent(inout) :: this
        class(tensor), intent(inout) :: c
        class(tensor), intent(in) :: a, b
        type(scalar), intent(in), optional :: alpha, beta
        type(stream), intent(in), optional :: astream

        class(tensor), allocatable :: ct, at, bt

        call this%create_permuted_tensors(c, a, b, ct, at, bt, &
                this%should_i_copy_c(beta), global_scratch_buffer, astream)
        call this%perform_contraction(ct, at, bt, alpha, beta, astream)
        call this%update_result(c, ct, astream)
        call this%destroy_permuted_tensors(ct, at, bt, global_scratch_buffer, astream)
    end subroutine contract

    subroutine synchronize(this, astream)
        class(global_buffered_ttgt_tc_driver), intent(inout) :: this
        type(stream), intent(in), optional :: astream

        call this%tc_ttgt%synchronize(astream)
        call global_scratch_buffer%clear_all_checkpoints()
    end subroutine synchronize
end module global_buffered_ttgt_tc_driver_module
