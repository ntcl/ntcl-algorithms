module tc_ttgt_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: data_api, only : &
            scratch_buffer, &
            stream

    use :: tensor_api, only : &
            tensor, &
            matrix, &
            scalar, &
            reshape_tensor, &
            tensor_builder

    use :: tensor_permute_module, only : tensor_permute
    use :: matrix_multiplication_module, only : matrix_multiplication

    use :: tensor_contraction_module, only : tensor_contraction
    use :: ttgt_descriptor_module, only : ttgt_descriptor

    implicit none
    private

    public :: tc_ttgt

    type, extends(tensor_contraction) :: tc_ttgt
        type(ttgt_descriptor) :: descr
        class(tensor_permute), allocatable :: permuter
        class(matrix_multiplication), allocatable :: mm
        type(tensor_builder) :: builder
    contains
        procedure :: contract => contract
        procedure :: contract_using_scratch => contract_using_scratch
        procedure :: synchronize => synchronize
        procedure :: cleanup => cleanup

        procedure :: setup => setup
        procedure :: create_permuted_tensors => create_permuted_tensors
        procedure :: perform_contraction => perform_contraction
        procedure :: update_result => update_result
        procedure :: destroy_permuted_tensors => destroy_permuted_tensors
        procedure :: create_permuted_tensor => create_permuted_tensor
        procedure :: allocate_tensor => allocate_tensor
        procedure :: allocate_tensor_in_scratch => allocate_tensor_in_scratch
        procedure :: reshape_to_matrices => reshape_to_matrices
        procedure :: destroy_permuted_tensor => destroy_permuted_tensor
        procedure :: clear => clear

        procedure, nopass :: should_i_copy_c => should_i_copy_c
    end type tc_ttgt
contains
    subroutine contract(this, c, a, b, alpha, beta, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: c
        class(tensor), intent(in) :: a, b
        type(scalar), intent(in), optional :: alpha, beta
        type(stream), intent(in), optional :: astream

        class(tensor), allocatable :: ct, at, bt

        call this%create_permuted_tensors(c, a, b, ct, at, bt, &
                this%should_i_copy_c(beta), astream=astream)
        call this%perform_contraction(ct, at, bt, alpha, beta, astream)
        call this%update_result(c, ct, astream)
        call this%destroy_permuted_tensors(ct, at, bt, astream=astream)
    end subroutine contract

    subroutine contract_using_scratch(this, c, a, b, scratch, alpha, beta, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: c
        class(tensor), intent(in) :: a, b
        type(scratch_buffer), intent(inout) :: scratch
        type(scalar), intent(in), optional :: alpha, beta
        type(stream), intent(in), optional :: astream

        class(tensor), allocatable :: ct, at, bt
        type(matrix) :: cm, am, bm
        integer(int64), dimension(3) :: mdims

        call this%create_permuted_tensors( &
                c, a, b, ct, at, bt, this%should_i_copy_c(beta), scratch, astream)

        call this%perform_contraction(ct, at, bt, alpha, beta, astream)
        call this%update_result(c, ct, astream)
        call this%destroy_permuted_tensors(ct, at, bt, scratch, astream)
    end subroutine contract_using_scratch

    subroutine synchronize(this, astream)
        class(tc_ttgt), intent(inout) :: this
        type(stream), intent(in), optional :: astream

        call this%mm%synchronize(astream)
        call this%permuter%synchronize(astream)
    end subroutine synchronize

    subroutine cleanup(this)
        class(tc_ttgt), intent(inout) :: this

        call this%synchronize()
        if (allocated(this%permuter)) then
            call this%permuter%cleanup()
            deallocate(this%permuter)
        end if

        if (allocated(this%mm)) then
            call this%mm%cleanup()
            deallocate(this%mm)
        end if

        call this%builder%cleanup()

        call this%clear()
    end subroutine cleanup

    subroutine create_permuted_tensors(this, c, a, b, ct, at, bt, copy_c, scratch, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(in) :: c, a, b
        class(tensor), allocatable, intent(inout) :: ct, at, bt
        logical, intent(in) :: copy_c
        type(scratch_buffer), intent(inout), optional :: scratch
        type(stream), intent(in), optional :: astream

        call this%create_permuted_tensor(c, ct, this%descr%corder, &
                copy_c, this%descr%permute_c, scratch, astream)
        call this%create_permuted_tensor(a, at, this%descr%aorder, &
                .true., this%descr%permute_a, scratch, astream)
        call this%create_permuted_tensor(b, bt, this%descr%border, &
                .true., this%descr%permute_b, scratch, astream)
    end subroutine create_permuted_tensors

    subroutine perform_contraction(this, ct, at, bt, alpha, beta, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: ct
        class(tensor), intent(in) :: at, bt
        type(scalar), intent(in), optional :: alpha, beta
        type(stream), intent(in), optional :: astream

        type(matrix) :: cm, am, bm

        call this%reshape_to_matrices(cm, am, bm, ct, at, bt)
        call this%mm%mm(cm, am, bm, alpha, beta, astream)
        call cm%release(); call am%release(); call bm%release()
    end subroutine perform_contraction

    subroutine update_result(this, c, ct, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: c
        class(tensor), intent(in) :: ct
        type(stream), intent(in), optional :: astream

        if ( this%descr%permute_c ) &
            call this%permuter%permute(ct, c, this%descr%cinverseorder, astream)
    end subroutine update_result

    subroutine destroy_permuted_tensors(this, c, a, b, scratch, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: c, a, b
        type(scratch_buffer), intent(inout), optional :: scratch
        type(stream), intent(in), optional :: astream

        call this%destroy_permuted_tensor(b, this%descr%permute_b, scratch)
        call this%destroy_permuted_tensor(a, this%descr%permute_a, scratch)
        call this%destroy_permuted_tensor(c, this%descr%permute_c, scratch)

        if ( present(scratch) ) &
                call scratch%checkpoint(astream)
    end subroutine destroy_permuted_tensors

    subroutine create_permuted_tensor(this, src, dst, order, copy, permute, scratch, astream)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(in) :: src
        class(tensor), allocatable, intent(inout) :: dst
        integer, dimension(:), intent(in) :: order
        logical, intent(in) :: copy, permute
        type(scratch_buffer), intent(inout), optional :: scratch
        type(stream), intent(in), optional :: astream

        if ( permute ) then
            if ( present(scratch) ) then
                call this%allocate_tensor_in_scratch(dst, scratch, src, order, .not. copy)
            else
                call this%allocate_tensor(dst, src, order, .not. copy)
            end if

            if (copy) &
                call this%permuter%permute(src, dst, order, astream)
        else
            dst = src
        end if
    end subroutine create_permuted_tensor

    subroutine allocate_tensor(this, dst, src, order, initialize)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), allocatable, intent(inout) :: dst
        class(tensor), intent(in) :: src
        integer, dimension(:), intent(in) :: order
        logical, intent(in) :: initialize

        call this%builder%allocate_and_create(dst, src%datatype, src%dims(order), initialize)
    end subroutine allocate_tensor

    subroutine allocate_tensor_in_scratch(this, dst, scratch, src, order, initialize)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), allocatable, intent(inout) :: dst
        type(scratch_buffer), intent(inout) :: scratch
        class(tensor), intent(in) :: src
        integer, dimension(:), intent(in) :: order
        logical, intent(in) :: initialize

        call this%builder%allocate_and_create_in_scratch(dst, scratch, src%datatype, src%dims(order), initialize)
    end subroutine allocate_tensor_in_scratch

    subroutine reshape_to_matrices(this, cm, am, bm, ct, at, bt)
        class(tc_ttgt), intent(inout) :: this
        type(matrix), intent(inout) :: cm, am, bm
        class(tensor), intent(in) :: ct, at, bt

        integer(int64), dimension(3) :: mdims

        mdims = this%descr%get_matrix_dimensions_from_permuted(at, bt)

        am = matrix(); bm = matrix(); cm = matrix()
        call reshape_tensor(cm, ct, [mdims(1), mdims(2)])
        if ( this%descr%switch_ab) then
            call reshape_tensor(am, bt, [mdims(1), mdims(3)])
            call reshape_tensor(bm, at, [mdims(3), mdims(2)])
        else
            call reshape_tensor(am, at, [mdims(1), mdims(3)])
            call reshape_tensor(bm, bt, [mdims(3), mdims(2)])
        end if
    end subroutine reshape_to_matrices

    subroutine destroy_permuted_tensor(this, dst, permute, scratch)
        class(tc_ttgt), intent(inout) :: this
        class(tensor), intent(inout) :: dst
        logical, intent(in) :: permute
        type(scratch_buffer), intent(inout), optional :: scratch

        if ( permute ) then
            if ( present(scratch) ) then
                call scratch%destroy(dst%storage)
            else
                call dst%cleanup()
            endif
        endif
        call dst%release()
    end subroutine destroy_permuted_tensor

    subroutine setup(this, descr, permuter, mm)
        class(tc_ttgt), intent(inout) :: this
        type(ttgt_descriptor), intent(in) :: descr
        class(tensor_permute), intent(in) :: permuter
        class(matrix_multiplication), intent(in) :: mm

        this%descr = ttgt_descriptor()
        this%permuter = permuter
        this%mm = mm
    end subroutine setup

    pure logical function should_i_copy_c(beta)
        type(scalar), intent(in), optional :: beta

        should_i_copy_c = .false.
        if ( present(beta) ) &
                should_i_copy_c = .not. beta%is_zero()
    end function should_i_copy_c

    subroutine clear(this)
        class(tc_ttgt), intent(inout) :: this

    end subroutine clear
end module tc_ttgt_module
